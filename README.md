Grafana
=======

# Description
This is the Grafana role for installing a Grafana (Bitnami) helm chart.

This chart installs:
* One Grafana instance
* One Grafana image-renderer
* A 10GB PV, it uses the default configured storage class when not defined
* An ingress

The Bitnami chart has many options. The features below are made usable through this role:

* Define a list of datasources (only Prometheus is supported)
* Use the certManager for creating tls certificates
* Enabled persistent storage
* Deploy image-renderer

For future implementation:
* Define a list of notifiers (Mattermost, Slack)
* Define preconfigured dashboards

# Prerequisites
* A Kubernetes-cluster (Kind, minikube, Openshift)
* Persistent storage enabled
* Ingress-controller
* Certmanager installed (optional)

# Role variables

## Custom grafana image repository

If you want to use your own image registry or point to a docker proxy use the following config items:

| config | default | example |
| --- | --- | --- |
| `config.grafana.image_registry` | empty, value from chart is used (docker.io) | my.docker-hub.proxy.local/docker-hub-proxy |
| `config.grafana.image_repo` | empty, value from chart is used (bitnami/grafana) | bitnami/my-grafana |
| `config.grafana.image_tag` | empty, value from chart is used | 1.2.3 |

## Extra pod labels

In case you need extra labels on the Grafana pod in your environment you can add them using the following: 

| config | default | example |
| --- | --- | --- |
| `config.grafana.extra_pod_labels` | `{}` | `{"my-label": "value1", "other-label": "value2"}` |

# Task structure

# Usage

# Sources

# Documentation
github page: https://github.com/bitnami/charts/tree/master/bitnami/grafana
Grafana documentation: https://grafana.com/docs/grafana/latest/

# TBD
* add oauth (oidc) authentication
* add default dashboards

# Extra Notes

# Debugging
